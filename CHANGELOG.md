## [3.6.4](https://gitlab.com/openpatch/flask-microservice-base/compare/v3.6.3...v3.6.4) (2020-07-06)


### Bug Fixes

* remove werkzeug dependency ([8382265](https://gitlab.com/openpatch/flask-microservice-base/commit/83822650e196cc69e84b7f4407bdf2fadd3c24b0))
